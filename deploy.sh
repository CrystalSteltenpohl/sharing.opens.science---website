### In Plesk, execute with:
### bash deploy.sh >> deployment.log 2>&1
### Cran job for executing every hour with logging
### @hourly bash ~/git_clone_hpss/deploy.sh >> ~/git_clone_hpss/deployment.log 2>&1
### https://www.cyberciti.biz/faq/how-do-i-add-jobs-to-cron-under-linux-or-unix-oses/
### To edit the cron file with nano instead of vim:
### export VISUAL=nano; crontab -e

echo ----------
echo $(date)

echo Showing and resetting path ...

echo Path: $PATH

### Set the path so LaTeX can be found
PATH=$PATH:/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin

echo Path: $PATH

### Go to directory with cloned git repo
cd ~/git_clone_sharing.opens.science

echo Done showing and resetting path. Running Quarto...

### Render the site
/usr/local/bin/quarto render

echo Done running Quarto. Deleting old files...

### Delete all contents in public HTML directory
rm -rf ~/httpdocs/*.*
rm -rf ~/httpdocs/*
rm -f ~/httpdocs/.htaccess

echo Done deleting old files. Copying new content...

### Copy website
cp -RT public ~/httpdocs

### Copy .htaccess
cp .htaccess ~/httpdocs

echo Done copying new content. Done.

echo ----------
